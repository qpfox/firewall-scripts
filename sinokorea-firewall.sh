#!/bin/bash
# TODO: Check for run as root, cause the old rules to be reloaded with service iptables restart if curling the new one fails, allow argument for name of chain
# Also since there are three different scripts available--China only, Korea only, China & Korea, offer option to change curled script via argument
# Running this as a cron job once per week is sufficient
iptables -F SINOKOREA
iptables -X SINOKOREA
iptables -N SINOKOREA
curl http://www.okean.com/antispam/iptables/rc.firewall.sinokorea | sed  -e 's/#.*//' -e 's/INPUT/SINOKOREA/g' -e '/^$/ d' -e '/state/d' -e '/||/d' | while read LINE ; do
	`$LINE`
done
